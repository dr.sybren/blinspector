import pathlib
import typing

import attr
import svn.local


@attr.s
class Change:
    op: str = attr.ib()
    """The performed operation (A, M, D, etc.)"""

    path: pathlib.Path = attr.ib()
    """The path of the file that changed"""


class LocalClient(svn.local.LocalClient):
    def __init__(self, path_, *args, **kwargs):
        if isinstance(path_, pathlib.Path):
            path_ = str(path_)
        super().__init__(path_, *args, **kwargs)

    def update_to_rev(self, revision: int) -> typing.List[Change]:
        output = self.run_command(
            'update',
            ['--revision', f'{revision}'],
            wd=self.path)

        changes = []
        for line in output[1:-1]:
            op, filename = line.split(maxsplit=1)
            change = Change(op, pathlib.Path(filename))
            changes.append(change)

        return changes

    def last_revision(self) -> int:
        info = self.info('@HEAD')
        revision = info['commit_revision']
        return revision

    def cleanup(self):
        self.run_command('cleanup', [], wd=self.path)
