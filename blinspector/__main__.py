import argparse
import datetime
import logging
import mimetypes
from pathlib import Path

from . import project_state_to_json as pstj
from . import svn, elastic

log = logging.getLogger(__name__)


def main():
    start_time = datetime.datetime.utcnow()
    try:
        args = parse_args()

        loglevel = logging.DEBUG if args.verbose else logging.INFO
        logging.basicConfig(level=loglevel,
                            format='%(asctime)-15s %(levelname)8s %(name)s %(message)s')
        mimetypes.add_type('application/x-blend', '.blend')

        # Set some global variables
        pstj.svn_root = svn_root = parse_svn_root(args)
        elastic.project_name = args.projname
        elastic.elastic_url = args.elastic

        svn_client = pstj.svn_client = svn.LocalClient(svn_root)

        first_rev, last_rev = parse_revision_range(args, svn_client)

        svn_client.update_to_rev(first_rev - 1)
        for rev in range(first_rev, last_rev + 1):
            pstj.describe_revision(rev, bam=args.bam)

            # Running cleanup removes superfluous files from .svn/pristene,
            # and thus makes out-of-space errors less likely.
            svn_client.cleanup()
    finally:
        end_time = datetime.datetime.utcnow()
        log.info('Total duration: %s', end_time - start_time)


def parse_revision_range(args, svn_client) -> (int, int):
    rev_range = tuple(x if x == 'HEAD' else int(x)
                      for x in args.revision.split(':'))
    first_rev = 0 if rev_range[0] == 'HEAD' else rev_range[0]

    last_rev = svn_client.last_revision() if rev_range[1] == 'HEAD' else rev_range[1]

    log.info('Revision range: %d-%d', first_rev, last_rev)
    return first_rev, last_rev


def parse_svn_root(args) -> Path:
    svn_root = args.root
    if not svn_root:
        svn_root = pstj.find_svn_root(args.svndir)

    log.info('SVN root: %s', svn_root)
    return svn_root


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('svndir', type=Path, help='SVN work directory to inspect.')
    parser.add_argument('projname', type=str,
                        help='Name of the project to include in all JSON sent to ElasticSearch.')
    parser.add_argument('--root', type=Path,
                        help='SVN root directory of the project, autodetected if not given.')
    parser.add_argument('-r', '--revision', default='1:HEAD', type=str,
                        help='Revision range to operate on')
    parser.add_argument('-e', '--elastic', default='http://localhost:9200/blinspector/', type=str,
                        help='ElasticSearch server URL, including the index name.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Enable debug logging.')
    parser.add_argument('-b', '--bam', action='store_true',
                        help='Enable dependency collection using BAM.')
    args = parser.parse_args()

    args.svndir = args.svndir.absolute()

    return args


if __name__ == '__main__':
    main()
