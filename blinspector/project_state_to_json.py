#!/usr/bin/env python3

import datetime
import logging
from pathlib import Path
import re
import typing

import attr
import mimetypes

import blinspector.svn
import blinspector.elastic

log = logging.getLogger(__name__)

# Some global state that I don't want to pass around all the time.
# It's all set from the main() function in __main__.py.
svn_root: Path = None
svn_client: blinspector.svn.LocalClient = None

# Matches lines like "+++ scenes/layout/layout.blend  (revision 9)"
diff_marker_line_re = re.compile(rb'^\+\+\+ .+[0-9]\)$', re.MULTILINE)

# Just matches the '+++' marker, to see if there is one at all in case the above regexp fails.
diff_marker_re = re.compile(rb'^\+\+\+', re.MULTILINE)


@attr.s
class CommitInfo:
    commit_rev: int = attr.ib()
    commit_date: datetime.datetime = attr.ib()
    commit_author: str = attr.ib()


@attr.s
class RevisionInfo(CommitInfo):
    commit_size_in_bytes: int = attr.ib()
    files_added: typing.List[Path] = attr.ib(default=attr.Factory(list))
    files_deleted: typing.List[Path] = attr.ib(default=attr.Factory(list))
    files_updated: typing.List[Path] = attr.ib(default=attr.Factory(list))
    commit_log: dict = attr.ib(default=attr.Factory(dict))

    @staticmethod
    def attrs_asdict_filter(attr, value) -> bool:
        """Removes empty lists"""
        if isinstance(value, list):
            return bool(value)
        return True

    @property
    def id_for_elastic(self):
        return f'{self.commit_rev}'


@attr.s
class FileInfo(CommitInfo):
    path: Path = attr.ib()
    depends: typing.List[Path] = attr.ib()
    mime_type: str = attr.ib()
    file_size_in_bytes: int = attr.ib()
    diff_size_in_bytes: int = attr.ib()

    @staticmethod
    def attrs_asdict_filter(attr, value) -> bool:
        """Removes None values"""
        return value is not None

    @property
    def id_for_elastic(self):
        # This is not really URL-safe, but good enough for our repositories.
        urlsafe_path = str(self.path).replace('/', '-')
        return f'{self.commit_rev}-{urlsafe_path}'


def find_svn_root(some_path: Path) -> Path:
    """Returns the absolute SVN root directory, raises ValueError if not found."""
    if not some_path.is_absolute():
        return find_svn_root(some_path.absolute())

    if not some_path.is_dir():
        return find_svn_root(some_path.parent)

    svn_dir = some_path / '.svn'
    if svn_dir.is_dir():
        return some_path

    if not some_path.parents:
        raise ValueError('Unable to find SVN root')

    return find_svn_root(some_path.parent)


def blend_deps(blend: Path) -> typing.Iterable[Path]:
    """Generator, yields dependencies of the given blend file."""
    from bam.blend import blendfile_path_walker

    blendfile_src = str(blend).encode('utf-8')

    vfb = blendfile_path_walker.FilePath.visit_from_blend
    for fp, _ in vfb(blendfile_src, readonly=True, recursive=False):
        f_abs = fp.filepath_absolute

        dep_path = Path(f_abs.decode('utf8'))
        if not dep_path.exists():
            log.debug('Skipping non-existent dependency %s', fp.filepath.decode('utf8'))
            continue
        yield dep_path


def diff_size(fullpath: Path, change_rev: int) -> int:
    """Returns the size of the diff, in bytes."""

    diff_result: bytes = svn_client.run_command(
        'diff',
        ['--change', f'{change_rev}', '--force', str(fullpath)],
        return_binary=True)

    # Find start of the change description.
    match = diff_marker_line_re.search(diff_result, endpos=1024)
    if not match:
        if not diff_marker_re.search(diff_result, endpos=1024):
            # There is not even a '+++' marker in the diff, so that means that the diff
            # is empty.
            return 0
        # Our initial regex failed, but there is a '+++' marker. This shouldn't be
        # silently ignored, as it can indicate a mistake in the regexp.
        raise ValueError(f'Unable to parse diff for revision {change_rev} of file {fullpath}')

    # Assume that the remainder of the output is a diff.
    return len(diff_result) - match.end() - 1


def current_revision_info() -> RevisionInfo:
    """Returns SVN info for the currently checked-out commit."""

    info = svn_client.info()
    revision = info['commit_revision']

    # Get the path to the repository as Path.
    repo_path_url = info['repository_root']
    assert repo_path_url.startswith('file://')
    repo_path = Path(repo_path_url[7:])

    commit_size_in_bytes = commit_size(repo_path, revision)

    commit_log = '\n'.join(entry.msg for entry in svn_client.log_default(limit=1)
                           if entry.msg is not None)
    return RevisionInfo(
        commit_size_in_bytes=commit_size_in_bytes,
        commit_rev=revision,
        commit_date=info['commit_date'],
        commit_author=info['commit_author'],
        commit_log=commit_log,
    )


def commit_size(repo_path: Path, revision: int) -> int:
    """Returns the on-disk size of the commit, in bytes"""

    # Not sure if the order of lines in this file is fixed, but in the
    # repos I've seen so far, it is.
    with open(repo_path / 'db/format') as fmtfile:
        fmt_version = fmtfile.readline()
        log.debug('Repository %s has format version %s', fmt_version)

        fmt_layout = fmtfile.readline().strip()
        layout_words = fmt_layout.split()
        assert layout_words[0] == 'layout'

        repo_layout = layout_words[1:]

    # Figure out the path of the commit file depending on the repository layout.
    revs_path = repo_path / 'db/revs'
    if repo_layout[0] == 'linear':
        rev_path = revs_path / str(revision)
    elif repo_layout[0] == 'sharded':
        shard_size = int(repo_layout[1])
        shard = revision // shard_size
        rev_path = revs_path / str(shard) / str(revision)
    else:
        raise ValueError(f'Unknown layout {fmt_layout} of repository {repo_path}')

    commit_size_in_bytes = rev_path.stat().st_size
    return commit_size_in_bytes


def relative_to_root(path: Path) -> Path:
    """Returns the path relative to the SVN root.

    If the path is not contained within the SVN root, returns
    the absolute path instead.
    """

    try:
        return path.relative_to(svn_root)
    except ValueError:
        return path


def file_info(relpath: Path, *, bam: bool) -> FileInfo:
    """Obtains info for a changed file.

    Assumes that this function is only called whenever the file was
    committed. Failing this assumption means that the diff size will
    be reported multiple times.
    """

    log.info('Inspecting %s', relpath)
    abspath = svn_root / relpath
    assert abspath.is_file()

    if bam and relpath.suffix.lower() == '.blend':
        depends = [relative_to_root(path) for path in blend_deps(abspath)]
    else:
        depends = None

    mime_type, encoding = mimetypes.guess_type(abspath.as_uri())

    svn_info = svn_client.info(str(relpath))
    file_diff_size = diff_size(abspath, svn_info['commit_revision'])

    info = FileInfo(
        path=relpath,
        file_size_in_bytes=abspath.stat().st_size,
        depends=depends,
        mime_type=mime_type,
        commit_rev=svn_info['commit_revision'],
        commit_date=svn_info['commit_date'],
        commit_author=svn_info['commit_author'],
        diff_size_in_bytes=file_diff_size,
    )

    return info


def describe_revision(rev: int, *, bam: bool):
    log.info('Describing revision %d', rev)
    changes = svn_client.update_to_rev(rev)

    commit = current_revision_info()
    op_lists = {
        'A': commit.files_added,  # Added
        'E': commit.files_added,  # Existed
        'D': commit.files_deleted,  # Deleted
        'U': commit.files_updated,  # Updated
        'G': commit.files_updated,  # Merged
        'R': commit.files_updated,  # Replaced
    }
    for change in changes:
        log.info(change)

        # NOTE: full directory deletions will not be reported accurately, as
        # SVN only seems to report the deletion of the directory, and not
        # the deletion of each file that was contained in it.
        abspath = svn_root / change.path
        if not abspath.is_file():
            log.info('Skipping non-file %s', change.path)
            continue

        op_lists[change.op].append(change.path)
        if change.op == 'D':
            continue

        info = file_info(change.path, bam=bam)
        blinspector.elastic.push_to_elastic('file', info.id_for_elastic, info)

    blinspector.elastic.push_to_elastic('commit', commit.id_for_elastic, commit)
