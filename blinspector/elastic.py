import datetime
import json
import logging
import typing
from pathlib import Path
import urllib.parse

import attr
import requests

log = logging.getLogger(__name__)
session = requests.Session()

# Some global state that I don't want to pass around all the time.
# It's all set from the main() function in __main__.py.
elastic_url: str = None
project_name: str = None


class ElasticError(Exception):
    """Raised when there was an error pushing a document to ElasticSearch"""


def jsonify(document, **kwargs) -> str:
    """Converts the document to JSON suitable for pushing to ElasticSearch.

    Also sets extra properties (like 'project_name') in the document.
    """
    assert project_name

    class JSONEncoder(json.JSONEncoder):
        """JSON encoder that supports Path objects (and others we need)."""

        def default(self, o):
            if isinstance(o, Path):
                return str(o)
            if isinstance(o, datetime.datetime):
                return o.isoformat()

            return super().default(o)

    # Convert any attrs-annotated type to a dictionary.
    if hasattr(document.__class__, '__attrs_attrs__'):
        attrs_filter = getattr(document.__class__, 'attrs_asdict_filter', None)
        document = attr.asdict(document, filter=attrs_filter)

    # Extend the document
    document['project_name'] = project_name

    kwargs.setdefault('sort_keys', True)
    kwargs.setdefault('indent', 4)
    return json.dumps(document, cls=JSONEncoder, **kwargs)


def push_to_elastic(type_name: str, id_for_elastic: str, document: typing.Any):
    assert elastic_url

    type_and_id = f'{type_name}/{id_for_elastic}'
    final_url = urllib.parse.urljoin(elastic_url, type_and_id)

    as_json = jsonify(document)
    log.info('Pushing to %s: %s', final_url, as_json)

    resp = session.post(final_url, data=as_json,
                        headers={'Content-Type': 'application/json'})
    resp.raise_for_status()
    resp_json = resp.json()

    log.debug('Response JSON: %s', resp_json)
    if resp_json['result'] not in {'created', 'updated'}:
        raise ElasticError(f'Error pushing to elastic: {resp_json}')
