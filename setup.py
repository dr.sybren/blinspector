# -*- coding: utf-8 -*-
from setuptools import setup

import sys

if sys.version_info < (3,):
    print("Sorry, Python 2 is not supported")
    sys.exit(1)

setup(
    name='blinspector',
    version='0.1',
    license='GPLv2+',
    author='Sybren Stüvel',
    author_email='sybren@blender.studio',
    description='Blender SVN repo inspector',
    zip_safe=False,
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=['blinspector'],
    entry_points={
        'console_scripts': [
            'blinspector = blinspector.__main__:main',
        ],
    },
    install_requires=[
        'attrs>=17.2.0',
        'blender-bam>=1.1.7',
        'svn>=0.3.45',
    ],
)
